# Dependencies

This packages requires the [Lely Opensource CANOpen stack and Python DCF tools](https://opensource.lely.com/canopen/docs/installation/).

We use features that are not present in the current `.deb` packages, so the libraries **must** be installed from source.  Further, the Python modules generated by the standard `autoconf` build process are broken.  Instead, we suggest installing the Python modules with `pip`. 

A sample build script is provided in [`build_lely.sh`](build_lely.sh)

