#!/bin/sh

BUILD_DIR=build

## Install and prereqs
sudo apt install cython3 gcc make autoconf


mkdir -p $BUILD_DIR && cd $BUILD_DIR

git clone https://gitlab.com/lely_industries/lely-core.git
cd lely-core

## Build the C libraries
autoreconf -i
mkdir -p build && cd build
../configure --disable-python2 --disable-cython --disable-python3
make -j
sudo make install

sudo ldconfig

## Install the python module
cd ../python/can
sudo pip3 install .
cd ../dcf-tools
sudo pip3 install .
