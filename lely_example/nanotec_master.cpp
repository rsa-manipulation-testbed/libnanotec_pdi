#include <lely/coapp/fiber_driver.hpp>
#include <lely/coapp/master.hpp>
#include <lely/ev/loop.hpp>
#include <lely/io2/linux/can.hpp>
#include <lely/io2/posix/poll.hpp>
#include <lely/io2/sys/io.hpp>
#include <lely/io2/sys/sigset.hpp>
#include <lely/io2/sys/timer.hpp>

#include <iostream>

using namespace std::chrono_literals;
using namespace lely;

// This driver inherits from FiberDriver, which means that all CANopen event
// callbacks, such as OnBoot, run as a task inside a "fiber" (or stackful
// coroutine).
class MyDriver : public canopen::FiberDriver {
public:
  using FiberDriver::FiberDriver;

  static const uint16_t PDICtrlIdx = 0x2290;
  static const uint16_t PDICtrlSubIdx = 0x00; // uint8_t

  static const uint16_t PDICmdIdx = 0x2291;
  static const uint16_t PDICmdSetValue1SubIdx = 0x01; // int32_t
  static const uint16_t PDICmdSetValue2SubIdx = 0x02; // int16_t
  static const uint16_t PDICmdSetValue3SubIdx = 0x03; // int8_t
  static const uint16_t PDICmdSubIdx = 0x04;          // int8_t

  static const uint16_t PDIStatusIdx = 0x2292;
  static const uint16_t PDIStatusSubIdx = 0x01;      // int16_t
  static const uint16_t PDIStatusValueSubIdx = 0x01; // int32_t

  enum {
    PDICmd_NOP = 0,
    PDICmd_SwitchOff = 1,
    PDICmd_ClearError = 2,
    PDICmd_Quickstop = 3,
    PDICmd_ODRead = 14,
    PDICmd_ODWrite = 15,
    PDICmd_AutoSetup = 16,
    PDICmd_HomingCurrentPosition = 17,
    PDICmd_Homing = 18,
    PDICmd_AbsPosition = 20,
    PDICmd_RelPosition = 21,
    PDICmd_Velocity = 23,
    PDICmd_Torque = 25,
    PDICmd_Halt = 64,
    PDICmd_ToggleCmd = 128
  };

  enum {
    PDIStatus_Enabled = 0x01,
    PDIStatus_Warning = (0x01 << 1),
    PDIStatus_Fault = (0x01 << 2),
    PDIStatus_TargetReacher = (0x01 << 3),
    PDIStatus_FollowingError = (0x01 << 4),
    PDIStatus_LimitReached = (0x01 << 5),
    PDIStatus_QsHalt = (0x01 << 7),
    PDIStatus_HomingDone = (0x01 << 11),
    PDIStatus_AutosetupDone = (0x01 << 12),
    PDIStatus_ToggleCmd = (0x01 << 14),
    PDIStatus_Error = (0x01 << 15)
  };

private:
  // This function gets called when the boot-up process of the slave
  // completes. The 'st' parameter contains the last known NMT state of the
  // slave (typically pre-operational), 'es' the error code (0 on success),
  // and 'what' a description of the error, if any.
  void OnBoot(canopen::NmtState /*st*/, char es,
              const std::string &what) noexcept override {
    if (!es || es == 'L') {
      std::cout << "slave " << static_cast<int>(id()) << " booted sucessfully"
                << std::endl;
    } else {
      std::cout << "slave " << static_cast<int>(id())
                << " failed to boot: " << what << std::endl;
    }
  }

  // This function gets called during the boot-up process for the slave. The
  // 'res' parameter is the function that MUST be invoked when the configuration
  // is complete. Because this function runs as a task inside a coroutine, it
  // can suspend itself and wait for an asynchronous function, such as an SDO
  // request, to complete.
  void OnConfig(std::function<void(std::error_code ec)> res) noexcept override {
    try {
      // Perform a few SDO write requests to configure the slave. The
      // AsyncWrite() function returns a future which becomes ready once the
      // request completes, and the Wait() function suspends the coroutine for
      // this task until the future is ready.

      // Configure the slave to monitor the heartbeat of the master (node-ID 1)
      // with a timeout of 2000 ms.
      Wait(AsyncWrite<uint32_t>(0x1016, 1, (1 << 16) | 2000));
      // Configure the slave to produce a heartbeat every 1000 ms.
      Wait(AsyncWrite<uint16_t>(0x1017, 0, 1000));
      // Configure the heartbeat consumer on the master.
      ConfigHeartbeat(2000ms);

      configureNanotecPDO();

      // Check PDI
      uint8_t pdi_ctrl = Wait(AsyncRead<uint8_t>(PDICtrlIdx, PDICtrlSubIdx));
      std::cerr << "PDI-Ctrl: " << std::hex << pdi_ctrl << " ; PDI is "
                << (pdi_ctrl & 0x01 ? "active" : "inactive") << std::endl;

      // Try sending a NOP
      // const uint8_t val = PDICmd_NOP;
      // tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;

      // Try sending a zero velocity command
      const uint8_t val = PDICmd_Velocity;
      tpdo_mapped[PDICmdIdx][PDICmdSetValue1SubIdx] = 0;
      tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;

      // Wait(AsyncWrite<int8_t>(PDICmdIdx, PDICmdSubIdx, PDICmd_NOP));

      // Report success (empty error code).
      res({});
    } catch (canopen::SdoError &e) {
      // If one of the SDO requests resulted in an error, abort the
      // configuration and report the error code.
      res(e.code());
    }
  }

  // This function is similar to OnConfg(), but it gets called by the
  // AsyncDeconfig() method of the master.
  void
  OnDeconfig(std::function<void(std::error_code ec)> res) noexcept override {
    try {
      // Disable the heartbeat consumer on the master.
      ConfigHeartbeat(0ms);
      // Disable the heartbeat producer on the slave.
      Wait(AsyncWrite<uint16_t>(0x1017, 0, 0));
      // Disable the heartbeat consumer on the slave.
      Wait(AsyncWrite<uint32_t>(0x1016, 1, 0));
      res({});
    } catch (canopen::SdoError &e) {
      res(e.code());
    }
  }

  // This function gets called every time a value is written to the local object
  // dictionary of the master by an RPDO (or SDO, but that is unlikely for a
  // master), *and* the object has a known mapping to an object on the slave for
  // which this class is the driver. The 'idx' and 'subidx' parameters are the
  // object index and sub-index of the object on the slave, not the local object
  // dictionary of the master.
  void OnRpdoWrite(uint16_t idx, uint8_t subidx) noexcept override {

    std::cerr << "Got RPDO idx " << idx << " subidx "
              << static_cast<int>(subidx) << std::endl;

    if (idx == PDIStatusIdx) {
      if (subidx == PDIStatusSubIdx) {
        const int16_t pdi_status = rpdo_mapped[PDIStatusIdx][PDIStatusSubIdx];
        std::cerr << "    Status mask: " << pdi_status << std::endl;
      } else if (subidx == PDIStatusValueSubIdx) {
        const int32_t pdi_value =
            rpdo_mapped[PDIStatusIdx][PDIStatusValueSubIdx];
        std::cerr << "   Status value: " << pdi_value << std::endl;
      }
    }
    // if (idx == 0x4001 && subidx == 0) {
    //   // Obtain the value sent by PDO from object 4001:00 on the slave.
    //   uint32_t val = rpdo_mapped[0x4001][0];
    //   // Increment the value and store it to an object in the local object
    //   // dictionary that will be sent by TPDO to object 4000:00 on the slave.
    //   tpdo_mapped[0x4000][0] = ++val;
    // }
  }

  void configureNanotecPDO() {
    // Ensure the PDOs are configured as expected on the Nanotec
    // This needs to agree with the PDO configuration in C5-E1-09-testbed-z.dcf
    // Shame this is so manual

    // From the web, the steps are:
    //
    //   1. Disable the PDO by setting bit 31 (0x80000000) of the COB-ID (object
    //   1800h + PDO#, sub-index 1) to 1.
    //
    //   2. Disable the current mapping by setting the number of mapped objects
    //   (object 1A00h + PDO#, sub-index 0) to 0.
    //
    //   3. Modify the mapping by changing the values of the corresponding
    //   sub-indices in object 1A00h + PDO#.
    //
    //   4. Enable the mapping by setting sub-index 0 of object 1A00h + PDO# to
    //   the number of mapped objects.
    //
    //   5. Enable the PDO by setting bit 31 (0x80000000) of the COB-ID (object
    //   1800h + PDO#, sub-index 1) to 0.

    const uint32_t RPDO_Params = 0x1403;
    const uint32_t RPDO_Mapping = 0x1603;

    const uint32_t TPDO_Params = 0x1803;
    const uint32_t TPDO_Mapping = 0x1A03;

    {
      // Disable the PDO by setting bit 31 of the COB-ID
      auto pdo_cobid = Wait(AsyncRead<uint32_t>(RPDO_Params, 1));
      std::cout << "RPDO COB-ID: " << std::hex << pdo_cobid << std::endl;

      Wait(AsyncWrite<uint32_t>(RPDO_Params, 1, pdo_cobid | 0x80000000));

      // Disable the mapping by setting the number of mapped objects to 0
      Wait(AsyncWrite<uint8_t>(RPDO_Mapping, 0, 0));

      Wait(AsyncWrite<uint32_t>(RPDO_Mapping, 1,
                                0x22910120)); // PDI-SetValue 1; 32bits
      Wait(AsyncWrite<uint32_t>(RPDO_Mapping, 2,
                                0x22910210)); // PDI-SetValue 2; 16bits
      Wait(AsyncWrite<uint32_t>(RPDO_Mapping, 3,
                                0x22910308)); // PDI-SetValue 3; 8bits
      Wait(AsyncWrite<uint32_t>(RPDO_Mapping, 4, 0x22910408)); // PDI-Cmd; 8bits

      // // Enable the mapping by setting the number of mapped objects to 4
      Wait(AsyncWrite<uint8_t>(RPDO_Mapping, 0, 4));

      Wait(AsyncWrite<uint32_t>(RPDO_Params, 1, pdo_cobid & ~(0x80000000)));
    }

    {
      // Disable the PDO by setting bit 31 of the COB-ID
      auto pdo_cobid = Wait(AsyncRead<uint32_t>(TPDO_Params, 1));
      Wait(AsyncWrite<uint32_t>(TPDO_Params, 1, pdo_cobid | 0x80000000));

      // Disable the mapping by setting the number of mapped objects to 0
      Wait(AsyncWrite<uint8_t>(TPDO_Mapping, 0, 0));

      Wait(AsyncWrite<uint32_t>(TPDO_Mapping, 1,
                                0x22920110)); // PDI-Status; 16 bits
      Wait(AsyncWrite<uint32_t>(TPDO_Mapping, 2,
                                0x603f0010)); // Error core; 16 bits
      Wait(AsyncWrite<uint32_t>(TPDO_Mapping, 3,
                                0x22920220)); // PDI-ReturnValue; 32bits

      // Enable the mapping by setting the number of mapped objects to 4
      Wait(AsyncWrite<uint8_t>(TPDO_Mapping, 0, 3));

      Wait(AsyncWrite<uint32_t>(TPDO_Params, 1, pdo_cobid & ~(0x80000000)));
    }
  }
};

int main() {
  // Initialize the I/O library. This is required on Windows, but a no-op on
  // Linux (for now).
  io::IoGuard io_guard;

  // Create an I/O context to synchronize I/O services during shutdown.
  io::Context ctx;
  // Create an platform-specific I/O polling instance to monitor the CAN bus, as
  // well as timers and signals.
  io::Poll poll(ctx);
  // Create a polling event loop and pass it the platform-independent polling
  // interface. If no tasks are pending, the event loop will poll for I/O
  // events.
  ev::Loop loop(poll.get_poll());
  // I/O devices only need access to the executor interface of the event loop.
  auto exec = loop.get_executor();
  // Create a timer using a monotonic clock, i.e., a clock that is not affected
  // by discontinuous jumps in the system time.
  io::Timer timer(poll, exec, CLOCK_MONOTONIC);

  // Create a virtual SocketCAN CAN controller and channel, and do not modify
  // the current CAN bus state or bitrate.
  io::CanController ctrl("can1");
  io::CanChannel chan(poll, exec);

  chan.open(ctrl);

  // Create a CANopen master with node-ID 1. The master is asynchronous, which
  // means every user-defined callback for a CANopen event will be posted as a
  // task on the event loop, instead of being invoked during the event
  // processing by the stack.
  canopen::AsyncMaster master(timer, chan, "nanotec-master.dcf", "", 1);

  // Create a driver for the slave with node-ID 2.
  MyDriver driver(exec, master, 15);

  // Create a signal handler.
  io::SignalSet sigset(poll, exec);
  // Watch for Ctrl+C or process termination.
  sigset.insert(SIGHUP);
  sigset.insert(SIGINT);
  sigset.insert(SIGTERM);

  // Submit a task to be executed when a signal is raised. We don't care which.
  sigset.submit_wait([&](int /*signo*/) {
    // If the signal is raised again, terminate immediately.
    sigset.clear();
    // Tell the master to start the deconfiguration process for all nodes, and
    // submit a task to be executed once that process completes.
    master.AsyncDeconfig().submit(exec, [&]() {
      // Perform a clean shutdown.
      ctx.shutdown();
    });
  });

  // Start the NMT service of the master by pretending to receive a 'reset
  // node' command.
  master.Reset();

  // Run the event loop until no tasks remain (or the I/O context is shut down).
  loop.run();

  return 0;
}