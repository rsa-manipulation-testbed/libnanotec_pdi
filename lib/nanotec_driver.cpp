#include <lely/coapp/fiber_driver.hpp>
#include <lely/coapp/master.hpp>
#include <lely/ev/loop.hpp>
#include <lely/io2/linux/can.hpp>
#include <lely/io2/posix/poll.hpp>
#include <lely/io2/sys/io.hpp>
#include <lely/io2/sys/sigset.hpp>
#include <lely/io2/sys/timer.hpp>

#include <iomanip>
#include <iostream>

#include "libnanotec_pdi/nanotec_driver.h"

using namespace std::chrono_literals;
using namespace lely;

namespace libnanotec_pdi {

NanotecPDIDriver::NanotecPDIDriver(CanOpenMaster &master, int8_t can_id)
    : LoopDriver(master.master(), can_id), pos_callback_(), vel_callback_(),
      fault_callback_(), toggle_(true) {}

// This function gets called when the boot-up process of the slave
// completes. The 'st' parameter contains the last known NMT state of the
// slave (typically pre-operational), 'es' the error code (0 on success),
// and 'what' a description of the error, if any.
void NanotecPDIDriver::OnBoot(canopen::NmtState /*st*/, char es,
                              const std::string &what) noexcept {

  std::cout << "OnBoot" << std::endl;
  if (!es || es == 'L') {
    std::cout << "slave " << static_cast<int>(id()) << " booted sucessfully"
              << std::endl;
  } else {
    std::cout << "slave " << static_cast<int>(id())
              << " failed to boot: " << what << std::endl;
  }
}

// This function gets called during the boot-up process for the slave. The
// 'res' parameter is the function that MUST be invoked when the
// configuration is complete. Because this function runs as a task inside a
// coroutine, it can suspend itself and wait for an asynchronous function,
// such as an SDO request, to complete.
void NanotecPDIDriver::OnConfig(
    std::function<void(std::error_code ec)> res) noexcept {

  std::cout << "OnConfig" << std::endl;

  try {
    // Perform a few SDO write requests to configure the slave. The
    // AsyncWrite() function returns a future which becomes ready once the
    // request completes, and the Wait() function suspends the coroutine for
    // this task until the future is ready.

    // Configure the slave to monitor the heartbeat of the master (node-ID
    // 1) with a timeout of 2000 ms.
    // Wait(AsyncWrite<uint32_t>(0x1016, 1, (1 << 16) | 2000));

    // Disable the heartbeat
    Wait(AsyncWrite<uint32_t>(0x1016, 1, 0x00));

    // Configure the slave to produce a heartbeat every 1000 ms.
    Wait(AsyncWrite<uint16_t>(0x1017, 0, 1000));

    // Configure the heartbeat consumer on the master.
    ConfigHeartbeat(2000ms);

    configureNanotecPDO();

    // Check PDI
    uint8_t pdi_ctrl = Wait(AsyncRead<uint8_t>(PDICtrlIdx, PDICtrlSubIdx));
    std::cerr << "PDI-Ctrl: " << std::hex << static_cast<int>(pdi_ctrl)
              << " ; PDI is " << (pdi_ctrl & 0x01 ? "active" : "inactive")
              << std::endl;

    // Initialize position and velocity
    if (pos_callback_) {
      uint32_t pos = Wait(AsyncRead<uint32_t>(0x6064, 0));
      pos_callback_(pos);
    }

    // ... and velocity
    if (vel_callback_) {
      uint32_t vel = Wait(AsyncRead<uint32_t>(0x606C, 0));
      vel_callback_(vel);
    }

    // Report success (empty error code).
    res({});
  } catch (canopen::SdoError &e) {
    // If one of the SDO requests resulted in an error, abort the
    // configuration and report the error code.
    std::cerr << "In catch" << std::endl;
    res(e.code());
  }
}

// This function is similar to OnConfg(), but it gets called by the
// AsyncDeconfig() method of the master.
void NanotecPDIDriver::OnDeconfig(
    std::function<void(std::error_code ec)> res) noexcept {

  std::cout << "OnDeconfig" << std::endl;

  try {
    // Disable the heartbeat consumer on the master.
    ConfigHeartbeat(0ms);
    // Disable the heartbeat producer on the slave.
    Wait(AsyncWrite<uint16_t>(0x1017, 0, 0));
    // Disable the heartbeat consumer on the slave.
    Wait(AsyncWrite<uint32_t>(0x1016, 1, 0));
    res({});
  } catch (canopen::SdoError &e) {
    res(e.code());
  }
}

// This function gets called every time a value is written to the local
// object dictionary of the master by an RPDO (or SDO, but that is unlikely
// for a master), *and* the object has a known mapping to an object on the
// slave for which this class is the driver. The 'idx' and 'subidx'
// parameters are the object index and sub-index of the object on the slave,
// not the local object dictionary of the master.
void NanotecPDIDriver::OnRpdoWrite(uint16_t idx, uint8_t subidx) noexcept {
  std::cerr << "Got RPDO idx " << std::hex << idx << " subidx "
            << static_cast<int>(subidx) << std::endl;

  if (idx == PDIStatusIdx) {
    if (subidx == PDIStatusSubIdx) {
      pdi_status_ = rpdo_mapped[PDIStatusIdx][PDIStatusSubIdx];
      std::cerr << "    PDI Status: " << std::hex << std::setw(2) << pdi_status_
                << std::endl;

      if (pdi_status_ & PDIStatus_Enabled) {
        std::cerr << "     * Enabled" << std::endl;
      }

      if (pdi_status_ & PDIStatus_Warning) {
        std::cerr << "     * Warning" << std::endl;
      }

      if (pdi_status_ & PDIStatus_Fault) {
        std::cerr << "     * Fault" << std::endl;
      }

      if (pdi_status_ & PDIStatus_TargetReached) {
        std::cerr << "     * Target reached" << std::endl;
      }

      if (pdi_status_ & PDIStatus_FollowingError) {
        std::cerr << "     * Following error" << std::endl;
      }

    } else if (subidx == PDIStatusValueSubIdx) {
      const int32_t pdi_value = rpdo_mapped[PDIStatusIdx][PDIStatusValueSubIdx];
      std::cerr << "   PDI Status value: " << pdi_value << std::endl;
    }
  } else if (idx == 0x603F) {
    const uint16_t errcode = rpdo_mapped[0x603F][0];
    std::cerr << "    Fault code: 0x" << std::hex << errcode << std::endl;

    if ((pdi_status_ & PDIStatus_Fault) && fault_callback_) {
      fault_callback_(errcode);
    }

  } else if (idx == 0x6041) {
    const uint16_t val = rpdo_mapped[idx][subidx];
    std::cerr << "Status word: 0x" << std::hex << val << std::endl;
  } else if (idx == 0x6061) {
    const uint8_t val = rpdo_mapped[idx][subidx];
    std::cerr << "Status word: 0x" << std::hex << static_cast<int>(val)
              << std::endl;
  } else if (idx == 0x6064) {
    // Absolute position value
    const int32_t pos = rpdo_mapped[idx][subidx];
    std::cerr << "  ++ Current position: " << std::dec << pos << std::endl;

    if (pos_callback_)
      pos_callback_(pos);
  } else if (idx == 0x606C) {
    // Absolute position value
    const int32_t vel = rpdo_mapped[idx][subidx];
    std::cerr << "  ++ Current velocity: " << std::dec << vel << std::endl;

    if (vel_callback_)
      vel_callback_(vel);
  }
}

void NanotecPDIDriver::configPDO(uint16_t param_addr,
                                 std::vector<uint32_t> values) {

  const uint16_t mapping_addr = param_addr + 0x0200;

  try {
    // Disable the PDO by setting bit 31 of the COB-ID
    auto pdo_cobid = Wait(AsyncRead<uint32_t>(param_addr, 1));

    Wait(AsyncWrite<uint32_t>(param_addr, 1, pdo_cobid | 0x80000000));

    // Set the timing to 1 for both TPDO and RPDOs
    Wait(AsyncWrite<uint8_t>(param_addr, 2, 1));

    // Disable the mapping by setting the number of mapped objects to 0
    Wait(AsyncWrite<uint8_t>(mapping_addr, 0, 0));

    for (std::vector<uint32_t>::size_type i = 0; i < values.size(); i++) {
      Wait(AsyncWrite<uint32_t>(mapping_addr, i + 1,
                                std::forward<uint32_t>(values[i])));
    }

    // Enable the mapping by setting the number of mapped objects
    Wait(AsyncWrite<uint8_t>(mapping_addr, 0, values.size()));

    Wait(AsyncWrite<uint32_t>(param_addr, 1, pdo_cobid & ~(0x80000000)));
  } catch (canopen::SdoError &e) {
    std::cerr << "Error: " << e.what() << std::endl;
    ;
  }
}

void NanotecPDIDriver::configureNanotecPDO() {
  // Ensure the PDOs are configured as expected on the Nanotec
  // This needs to agree with the PDO configuration in
  // C5-E1-09-testbed-z.dcf Shame this is so manual

  // From the web, the steps are:
  //
  //   1. Disable the PDO by setting bit 31 (0x80000000) of the COB-ID
  //   (object 1800h + PDO#, sub-index 1) to 1.
  //
  //   2. Disable the current mapping by setting the number of mapped
  //   objects (object 1A00h + PDO#, sub-index 0) to 0.
  //
  //   3. Modify the mapping by changing the values of the corresponding
  //   sub-indices in object 1A00h + PDO#.
  //
  //   4. Enable the mapping by setting sub-index 0 of object 1A00h + PDO#
  //   to the number of mapped objects.
  //
  //   5. Enable the PDO by setting bit 31 (0x80000000) of the COB-ID
  //   (object 1800h + PDO#, sub-index 1) to 0.

  const uint32_t RPDO1_Params = 0x1400;
  const uint32_t RPDO2_Params = RPDO1_Params + 1;
  const uint32_t RPDO3_Params = RPDO1_Params + 2;
  const uint32_t RPDO4_Params = RPDO1_Params + 3;
  // const uint32_t RPDO_Mapping = 0x1603;

  const uint32_t TPDO1_Params = 0x1800;
  const uint32_t TPDO2_Params = TPDO1_Params + 1;
  const uint32_t TPDO3_Params = TPDO1_Params + 2;
  const uint32_t TPDO4_Params = TPDO1_Params + 3;
  // const uint32_t TPDO_Mapping = 0x1A03;

  Wait(AsyncWrite<uint8_t>(RPDO1_Params, 2, 1));
  Wait(AsyncWrite<uint8_t>(RPDO3_Params, 2, 1));

  // This configuration needs to agree with what's given in
  // eds/C5-E-1-09-testbed-zaxis.dcf

  configPDO(RPDO2_Params, {0x607A0020, 0x60810020});
  configPDO(RPDO4_Params, {0x22910120, 0x22910210, 0x22910308, 0x22910408});
  Wait(AsyncWrite<uint8_t>(RPDO4_Params, 2,
                           0xFF)); // Send PDI commands immediately

  // Position and velocity
  configPDO(TPDO2_Params, {0x60640020, 0x606C0020});
  Wait(AsyncWrite<uint8_t>(TPDO2_Params, 2,
                           0xFF)); // Send Position and velocity immediately

  // PDI status and errors
  configPDO(TPDO4_Params, {0x22920110, 0x603f0010, 0x22920220});
  Wait(AsyncWrite<uint8_t>(TPDO4_Params, 2,
                           0x0)); // Send PDI commands immediately
  // Wait(AsyncWrite<uint16_t>(TPDO4_Params, 3,
  //                           0x64)); // 10ms inhibit time...
}

void NanotecPDIDriver::configureNanotecLimits() {}

//==================================================================

uint8_t NanotecPDIDriver::addToggle(uint8_t cmd) {
  uint8_t out = cmd | (toggle_ ? PDICmd_ToggleCmd : 0);
  toggle_ = !toggle_;

  return out;
}

void NanotecPDIDriver::doNop() {
  try {
    const uint8_t val = addToggle(PDICmd_NOP);
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

void NanotecPDIDriver::doClearError() {
  try {
    std::cerr << "Sending clear error" << std::endl;
    const uint8_t val = addToggle(PDICmd_ClearError);
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

void NanotecPDIDriver::doSwitchOff() {
  try {
    const uint8_t val = addToggle(PDICmd_SwitchOff);
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

void NanotecPDIDriver::doAbsolutePosition(int32_t pos, int16_t max_vel) {
  // doNop();

  try {
    // std::cerr << "!! Setting absolute position " << pos << std::endl;

    // Try sending a zero velocity command
    const uint8_t val = addToggle(PDICmd_AbsPosition);

    tpdo_event_mutex.lock();
    tpdo_mapped[PDICmdIdx][PDICmdSetValue1SubIdx] = pos;
    tpdo_mapped[PDICmdIdx][PDICmdSetValue2SubIdx] = max_vel;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_event_mutex.unlock();
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

void NanotecPDIDriver::doRelativePosition(int32_t pos, int16_t max_vel) {
  // doNop();

  try {
    // std::cerr << "!! Setting relative position " << pos << std::endl;
    // Try sending a zero velocity command
    const uint8_t val = addToggle(PDICmd_RelPosition);

    tpdo_event_mutex.lock();
    tpdo_mapped[PDICmdIdx][PDICmdSetValue1SubIdx] = pos;
    tpdo_mapped[PDICmdIdx][PDICmdSetValue2SubIdx] = max_vel;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_event_mutex.unlock();
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

void NanotecPDIDriver::doVelocity(int32_t rpm) {
  // doNop();

  try {
    std::cerr << "!! Setting velocity " << std::dec << rpm << " rpm" << std::endl;
    const uint8_t val = addToggle(PDICmd_Velocity);

    tpdo_mapped[PDICmdIdx][PDICmdSetValue1SubIdx] = rpm;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx] = val;
    tpdo_mapped[PDICmdIdx][PDICmdSubIdx].WriteEvent();
  } catch (std::system_error &e) {
    std::cerr << "Error: " << e.what() << std::endl;
  }
}

} // namespace libnanotec_pdi
