#include "libnanotec_pdi/canopen_master.h"

// Static DCF is pulled in from an auto-generated file
extern "C" {
extern const struct co_sdev nanotec_dcf;
}

namespace libnanotec_pdi {

CanOpenMaster::CanOpenMaster(const std::string &can_dev, uint8_t master_id)
    : io_guard_(), ctx_(), poll_(ctx_), loop_(poll_.get_poll()),
      timer_(poll_, loop_.get_executor(), CLOCK_MONOTONIC),
      ctrl_(can_dev.c_str()), chan_(poll_, loop_.get_executor()),
      master_(loop_.get_executor(), timer_, chan_, &nanotec_dcf, master_id) {
  chan_.open(ctrl_);
}

CanOpenMaster::~CanOpenMaster() {}

void CanOpenMaster::run() {
  master_.Reset();

  loop_.run();
}

void CanOpenMaster::halt() {
  master_.AsyncDeconfig().submit(exec(), [&]() { ctx_.shutdown(); });
}

} // namespace libnanotec_pdi