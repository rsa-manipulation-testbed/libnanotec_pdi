#include <iostream>

#include "libnanotec_pdi/canopen_master.h"
#include "libnanotec_pdi/nanotec_driver.h"

using namespace std::chrono_literals;
using namespace lely;

using namespace libnanotec_pdi;

int main() {

  CanOpenMaster master("can1");
  NanotecPDIDriver driver(master, 15);

  // Create a signal handler.
  io::SignalSet sigset(master.poll(), master.exec());
  // Watch for Ctrl+C or process termination.
  sigset.insert(SIGHUP);
  sigset.insert(SIGINT);
  sigset.insert(SIGTERM);

  // Submit a task to be executed when a signal is raised. We don't care which.
  sigset.submit_wait([&](int /*signo*/) {
    // If the signal is raised again, terminate immediately.
    sigset.clear();

    master.halt();
    // // Tell the master to start the deconfiguration process for all nodes,
    // and
    // // submit a task to be executed once that process completes.
    // master.AsyncDeconfig().submit(exec, [&]() {
    //   // Perform a clean shutdown.
    //   ctx.shutdown();
    // });
  });

  master.run();

  return 0;
}