#pragma once

#include <lely/coapp/fiber_driver.hpp>
#include <lely/coapp/master.hpp>
#include <lely/ev/loop.hpp>
#include <lely/io2/linux/can.hpp>
#include <lely/io2/posix/poll.hpp>
#include <lely/io2/sys/io.hpp>
#include <lely/io2/sys/sigset.hpp>
#include <lely/io2/sys/timer.hpp>

#include <iostream>

namespace libnanotec_pdi {

using namespace std::chrono_literals;
using namespace lely;

class CanOpenMaster {
public:
  CanOpenMaster(const std::string &can_dev, uint8_t master_id = 1);

  ~CanOpenMaster();

  void run();
  void halt();

  io::Poll &poll() { return poll_; }
  ev_exec_t *exec() { return loop_.get_executor(); }
  canopen::AsyncMaster &master() { return master_; }

private:
  io::IoGuard io_guard_;
  io::Context ctx_;
  io::Poll poll_;
  ev::Loop loop_;
  io::Timer timer_;

  io::CanController ctrl_;
  io::CanChannel chan_;

  canopen::AsyncMaster master_;
};

} // namespace libnanotec_pdi