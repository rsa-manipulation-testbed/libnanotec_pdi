#pragma once

#include <lely/co/sdev.hpp>
#include <lely/coapp/loop_driver.hpp>
#include <lely/coapp/master.hpp>
#include <lely/ev/loop.hpp>
#include <lely/io2/linux/can.hpp>
#include <lely/io2/posix/poll.hpp>
#include <lely/io2/sys/io.hpp>
#include <lely/io2/sys/sigset.hpp>
#include <lely/io2/sys/timer.hpp>

#include <iostream>

#include "libnanotec_pdi/canopen_master.h"

extern "C" {
extern const struct co_sdev nanotec_dcf;
}

namespace libnanotec_pdi {

using namespace std::chrono_literals;
using namespace lely;

// This driver inherits from FiberDriver, which means that all CANopen event
// callbacks, such as OnBoot, run as a task inside a "fiber" (or stackful
// coroutine).
class NanotecPDIDriver : public canopen::LoopDriver {
public:
  using LoopDriver::LoopDriver;

  static const uint16_t PDICtrlIdx = 0x2290;
  static const uint16_t PDICtrlSubIdx = 0x00; // uint8_t

  static const uint16_t PDICmdIdx = 0x2291;
  static const uint16_t PDICmdSetValue1SubIdx = 0x01; // int32_t
  static const uint16_t PDICmdSetValue2SubIdx = 0x02; // int16_t
  static const uint16_t PDICmdSetValue3SubIdx = 0x03; // int8_t
  static const uint16_t PDICmdSubIdx = 0x04;          // int8_t

  static const uint16_t PDIStatusIdx = 0x2292;
  static const uint16_t PDIStatusSubIdx = 0x01;      // int16_t
  static const uint16_t PDIStatusValueSubIdx = 0x02; // int32_t

  enum {
    PDICmd_NOP = 0,
    PDICmd_SwitchOff = 1,
    PDICmd_ClearError = 2,
    PDICmd_Quickstop = 3,
    PDICmd_ODRead = 14,
    PDICmd_ODWrite = 15,
    PDICmd_AutoSetup = 16,
    PDICmd_HomingCurrentPosition = 17,
    PDICmd_Homing = 18,
    PDICmd_AbsPosition = 20,
    PDICmd_RelPosition = 21,
    PDICmd_Velocity = 23,
    PDICmd_Torque = 25,
    PDICmd_Halt = 64,
    PDICmd_ToggleCmd = 128
  };

  enum {
    PDIStatus_Enabled = 0x01,
    PDIStatus_Warning = (0x01 << 1),
    PDIStatus_Fault = (0x01 << 2),
    PDIStatus_TargetReached = (0x01 << 3),
    PDIStatus_FollowingError = (0x01 << 4),
    PDIStatus_LimitReached = (0x01 << 5),
    PDIStatus_QsHalt = (0x01 << 7),
    PDIStatus_HomingDone = (0x01 << 11),
    PDIStatus_AutosetupDone = (0x01 << 12),
    PDIStatus_ToggleCmd = (0x01 << 14),
    PDIStatus_Error = (0x01 << 15)
  };

  NanotecPDIDriver(CanOpenMaster &master, int8_t can_id);

  //=== Userspace commands to trigger various commands ==

  uint8_t addToggle(uint8_t cmd);

  void doNop();

  void doClearError();
  void doSwitchOff();

  // Positions are in the Nanotec controller's default units:
  //    - Positions in tenths of a degree (3600 per revolution)
  //    - Velocities in rev per minute
  void doAbsolutePosition(int32_t pos, int16_t max_vel);
  void doRelativePosition(int32_t pos, int16_t max_vel);
  void doVelocity(int32_t rpm);

  void doHalt() { doVelocity(0); }

  typedef std::function<void(int32_t)> PositionCallback;
  typedef PositionCallback VelocityCallback;

  typedef std::function<void(uint16_t)> FaultCallback;

  void setPositionCallback(PositionCallback f) { pos_callback_ = f; }
  void setVelocityCallback(VelocityCallback f) { vel_callback_ = f; }
  void setFaultCallback(FaultCallback f) { fault_callback_ = f; }

private:
  // This function gets called when the boot-up process of the slave
  // completes. The 'st' parameter contains the last known NMT state of the
  // slave (typically pre-operational), 'es' the error code (0 on success),
  // and 'what' a description of the error, if any.
  void OnBoot(canopen::NmtState /*st*/, char es,
              const std::string &what) noexcept override;

  // This function gets called during the boot-up process for the slave. The
  // 'res' parameter is the function that MUST be invoked when the
  // configuration is complete. Because this function runs as a task inside a
  // coroutine, it can suspend itself and wait for an asynchronous function,
  // such as an SDO request, to complete.
  void OnConfig(std::function<void(std::error_code ec)> res) noexcept override;

  // This function is similar to OnConfg(), but it gets called by the
  // AsyncDeconfig() method of the master.
  void
  OnDeconfig(std::function<void(std::error_code ec)> res) noexcept override;

  // This function gets called every time a value is written to the local
  // object dictionary of the master by an RPDO (or SDO, but that is unlikely
  // for a master), *and* the object has a known mapping to an object on the
  // slave for which this class is the driver. The 'idx' and 'subidx'
  // parameters are the object index and sub-index of the object on the slave,
  // not the local object dictionary of the master.
  void OnRpdoWrite(uint16_t idx, uint8_t subidx) noexcept override;

  void configPDO(uint16_t param_addr, std::vector<uint32_t> values);
  void configureNanotecPDO();

  // Configures Nanotec constants (min/max velocity, etc)
  void configureNanotecLimits();

  PositionCallback pos_callback_, vel_callback_;
  FaultCallback fault_callback_;
  bool toggle_;

  uint16_t pdi_status_;
};

} // namespace libnanotec_pdi
